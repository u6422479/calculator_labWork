package sa.hopkins.calculator;


import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.*;
import java.util.Objects;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity {

    public int one = 1;
    public int two = 2;
    public int three = 3;
    public int four = 4;
    public int five = 5;
    public int six = 6;
    public int seven = 7;
    public int eight = 8;
    public int nine = 9;
    public int zero = 0;
    public String closeB = ")";
    public String openB = "(";
    public String displayReset = "0";
    public String temp = "";
    public String input1 = "";
    public String input2 = "";
    public double sum1;
    public double sum2;
    public int action;
    public double result = 0.0;
    public String name = "history.txt";
    public static String first = "0";
    String output;
    String display = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        TextView Counter = (TextView) findViewById(R.id.counter);
        String display = Counter.getText().toString();

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_main_land);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main);
        }

        Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }


    /////////////////////number buttons///////////////////////////////
    public void oneB(View view) {

        display = display.concat(Integer.toString(one));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);

    }

    public void twoB(View view) {

        display = display.concat(Integer.toString(two));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void threeB(View view) {

        display = display.concat(Integer.toString(three));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);

    }

    public void fourB(View view) {
//        String display = "";
        display = display.concat(Integer.toString(four));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void fiveB(View view) {
//        String display = "";
        display = display.concat(Integer.toString(five));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void sixB(View view) {
//        String display = "";
        display = display.concat(Integer.toString(six));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void sevenB(View view) {
//        String display = "";
        display = display.concat(Integer.toString(seven));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void eightB(View view) {
//        String display = "";
        display = display.concat(Integer.toString(eight));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void nineB(View view) {
//        String display = "";
        display = display.concat(Integer.toString(nine));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void zeroB(View view) {
//        String display = "";
        display = display.concat(Integer.toString(zero));
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void closeBr(View view) {
//        String display = "";
        display = display.concat(closeB);
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void openBr(View view) {
//        String display = "";
        display = display.concat(openB);
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }




    public void countReset(View view) {
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(displayReset);
        display = new String("");
        input1 = new String(input1);
        input2 = new String(input2);
    }


    //***** i havent had the time to fix the persistent data call yet ******//
    public void return_value(View view) {
        TextView Counter = (TextView) findViewById(R.id.counter);
//        String display = (fileRead.first).toString();
        Counter.setText(display);
    }



///////////////////////////////number buttons///////////////////////////////////////


//////////////////////////////////////////operators////////////////////////////////////////////////////

//    public void plus(View view) {
//        TextView Counter = (TextView) findViewById(R.id.counter);
//        String display = Counter.getText().toString();
//        input1 += display;
//        action = 1;
//        Counter.setText(displayReset);
//    }
//
//    public void minus(View view) {
//
//        TextView Counter = (TextView) findViewById(R.id.counter);
//        String display = Counter.getText().toString();
//        input1 += display;
//        action = 2;
//        Counter.setText(displayReset);
//    }
//
//    public void divide(View view) {
//
//        TextView Counter = (TextView) findViewById(R.id.counter);
//        String display = Counter.getText().toString();
//        input1 += display;
//        action = 3;
//        Counter.setText(displayReset);
//
//    }
//
//    public void mult(View view) {
//
//        TextView Counter = (TextView) findViewById(R.id.counter);
//        String display = Counter.getText().toString();
//        input1 += display;
//        action = 4;
//        Counter.setText(displayReset);
//    }


    public void plus(View view) {
        display = display.concat("+");
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void minus(View view) {
        display = display.concat("-");
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

    public void divide(View view) {
        display = display.concat("/");
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);

    }

    public void mult(View view) {
        display = display.concat("*");
        TextView Counter = (TextView) findViewById(R.id.counter);
        Counter.setText(display);
    }

//////////////////////////////////////////operators////////////////////////////////////////////////////


    public void equals(View view) {

        tokenizer tz = new simple_token(display);
        Object tok;
        while (tz.hasNext()) {
            tok = tz.current();
            System.out.println(tok instanceof String ? tok :
                    (tok instanceof Integer?"int:" + tok: "double:" + tok));
            tz.next();
        }


        baseExp exp = exp.parseExp(tz);

        System.out.println("finished equals");

//
//        TextView Counter = (TextView) findViewById(R.id.counter);
//        String display = Counter.getText().toString();
//        String test;
//        test = Double.toString(result);
//        input2 += display;
//
//        if (input2 == test) {
//            result = new Double(result);
//            countReset(view);
//        }
//
//        sum1 = Double.parseDouble(input1);
//        sum2 = Double.parseDouble(input2);
//        System.out.println("equals");
//
//        if (action == 1) {
//            result = sum1 + sum2;
//        }
//        if (action == 2) {
//            result = sum1 - sum2;
//        }
//        if (action == 3) {
//            result = sum1 / sum2;
//        }
//        if (action == 4) {
//            result = sum1 * sum2;
//        }
//        display = Double.toString(result);
//        Counter.setText(display);
//        System.out.println(result);
//        write();
//
//
//
//        sum1 = 0;
//        sum2 = 0;

    }


    public void write() {
        System.out.println("file writing started");
//        try {
//            FileOutputStream fou = openFileOutput(name, MODE_APPEND);
//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fou);
//            TextView Counter = (TextView) findViewById(R.id.counter);
//            String display = Counter.getText().toString();
//            outputStreamWriter.write(display);
//            outputStreamWriter.close();
//            System.out.println("file writing done");
//        } catch (IOException e) {
//            Log.e("Exception", "File write failed: " + e.toString());
//        }
        reading();
    }


    public void reading() {
        System.out.println("file read");
//        ListView listView = (ListView) findViewById(R.id.calc_history);
//        try {
//            InputStream inputreader = openFileInput(name);
//            BufferedReader buffreader = new BufferedReader(new InputStreamReader(inputreader));
//
//            ArrayList<String> lines = new ArrayList<String>();
//            boolean hasNextLine = true;
//            while (hasNextLine) {
//                String line = buffreader.readLi
// ne();
//                lines.add(line);
//                hasNextLine = line != null;
//            }
//
//            output = lines.get(0);
//            first = output;
//            System.out.println(lines.get(0));
//            System.out.println(lines.get(1));
//            System.out.println(lines.get(2));
//            inputreader.close();
//
//        } catch (java.io.FileNotFoundException e) {
//
//        } catch (java.io.IOException e) {
//
//        }
    }
}




