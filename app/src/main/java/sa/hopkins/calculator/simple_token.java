package sa.hopkins.calculator;
import java.io.StreamTokenizer;
/**
 * Created by scott on 23/05/2017.
 */
public class simple_token extends tokenizer {


//    <operator> ::+ "+" | "-" | "*" | "/"
//    <digit> ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
//
//    <exp> ::= <digit> | <operator> | <digit>


        private String text;
        private int pos;
        private Object current;

        static final char whitespace[] = { ' ', '\n', '\t' };
        static final char operator[] = { '(', ')', '+', '-', '/', '*', '+' };

    public simple_token(String text) {
            this.text = text;
            this.pos = 0;
            next();
        }
        boolean hasNext() {
            return current != null;
        }

        Object current() {
            return current;
        }

        public void next() {
            consumewhite();
            if (pos == text.length()) {
                current = null;
            }
            else if (isin(text.charAt(pos), operator)) {
                current = "" + text.charAt(pos);
                pos++;
            }
            else if (Character.isDigit(text.charAt(pos))) {
                int start = pos;
                while (pos < text.length() && Character.isDigit(text.charAt(pos)) )
                    pos++;
                if (pos+1 < text.length() && text.charAt(pos) == '.' &&
                        Character.isDigit(text.charAt(pos+1))) {
                    pos++;

                    while (pos < text.length() && Character.isDigit(text.charAt(pos)))
                        pos++;
                    current = Double.parseDouble(text.substring(start, pos));
                }
                else {
                    current = Integer.parseInt(text.substring(start, pos));
                }
            }
            else {
                int start = pos;
                while (pos < text.length() && !isin(text.charAt(pos), operator)
                        && !isin(text.charAt(pos), whitespace))
                    pos++;
                current = text.substring(start, pos);
            }
        }

        private void consumewhite() {
            while (pos < text.length() && isin(text.charAt(pos), whitespace))
                pos++;
        }
        private boolean isin(char c, char charlist[]) {
            for (char w : charlist) {
                if (w == c)
                    return true;
            }
            return false;
        }
}


