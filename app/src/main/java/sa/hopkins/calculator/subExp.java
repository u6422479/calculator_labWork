package sa.hopkins.calculator;

/**
 * Created by scott on 26/05/2017.
 */
public class subExp extends baseExp {

    baseExp exp;


    public subExp(baseExp exp){

        this.exp = exp;
    }

    public String show(){
        return "(" + left.show() + " - " + right.show() + ")";
    }

    public int solution(){

        return left.solution() - right.solution();

    }

}
