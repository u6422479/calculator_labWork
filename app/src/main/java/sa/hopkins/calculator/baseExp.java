package sa.hopkins.calculator;



import java.util.ArrayList;

/**
 * Created by scott on 26/05/2017.
 */
public abstract class baseExp {


    abstract public String show();
    abstract public int solution();

    public static baseExp parseExp(tokenizer tok) throws ParseException {
        Object t = tok.current();
        if (t instanceof Integer) {
            int v = (Integer) t;
            tok.next();
            return new litExp(v);
        } else if (t.equals("-")) {
            tok.next();
            tok.parse("(");
            baseExp exp = parseExp(tok);
            tok.parse(")");
            return new subExp(exp);
        } else if (t.equals("+")) {
            tok.next();
            tok.parse("(");
            baseExp exp = parseExp(tok);
            tok.parse(")");
            return new addExp(exp);
        } else if (t.equals("/")) {
            tok.next();
            tok.parse("(");
            baseExp exp = parseExp(tok);
            tok.parse(")");
            return new subExp(exp);
        } else if (t.equals("*")) {
            tok.next();
            tok.parse("(");
            baseExp exp = parseExp(tok);
            tok.parse(")");
            return new subExp(exp);
        } else if (t.equals("(")) {
            tok.next();
            baseExp bool = parseExp(tok);
            tok.parse("=");
            tok.parse("=");
            tok.parse(new Integer(0));
            tok.parse("?");
            baseExp exp1 = parseExp(tok);
            tok.parse(":");
            baseExp exp2 = parseExp(tok);
            tok.parse(")");
            return new Select(bool,exp1,exp2);
        } else if (t instanceof String && Character.isUpperCase(((String) t).charAt(0))) {
            tok.next();
            return new Variable((String) t);
        } else if (t instanceof String) {
            FunctionCall res = new FunctionCall();
            res.expressions = new ArrayList<baseExp>();
            res.name = (String) t;
            tok.next();
            tok.parse("(");
            String var;
            while (!")".equals(tok.current())) {
                res.expressions.add(parseExp(tok));
                if (tok.current().equals(",")) tok.next();
            }
            tok.parse(")");
            return res;
        } else {
            throw new ParseException();
        }
    }


}
