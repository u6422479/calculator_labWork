package sa.hopkins.calculator;


import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;

/**
 * Created by scott on 23/05/2017.
 */
public abstract class tokenizer {

    abstract boolean hasNext();
    abstract Object current();
    abstract void next();
    public void parse(Object o) throws ParseException{
        if (current() == null || !current().equals(o)) throw new ParseException();
        next();

    }





}
