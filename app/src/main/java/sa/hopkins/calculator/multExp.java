package sa.hopkins.calculator;

/**
 * Created by scott on 26/05/2017.
 */
public class multExp extends baseExp {

    baseExp left, right;


    public multExp(baseExp r, baseExp l){
        left = l;
        right = r;
    }

    public String show(){
        return "(" + left.show() + " * " + right.show() + ")";
    }

    public int solution(){

        return left.solution() * right.solution();

    }

}
